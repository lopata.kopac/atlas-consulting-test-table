import React from 'react';
import { useBreakpoints } from "react-use-breakpoints";
import Compoments from '../compoments';
import MobileLogo from '../assets/120x40.png';
import MainLogo from '../assets/220x155.png';

import './index.scss';

const data = [
  {
    id: '623',
    email: 'smlolar@freeman.com',
    name: 'Smolar',
    licence: 'Licence 1'
  },
  {
    id: '624',
    email: 'moro@freeman.com',
    name: 'Moro',
    licence: 'Licence 1'
  },
  {
    id: '625',
    email: 'smlolar@freeman.com',
    name: 'Lono',
    licence: 'Licence 2'
  }
];

function App() {
  const { breakpoint } = useBreakpoints();

  let isMobile = breakpoint === 'xs';

  const tableColumns = React.useMemo(
    () => [
      {
        Header: 'ID',
        accessor: 'id',
      },
      {
        Header: 'Email',
        accessor: 'email',
      },
      {
        Header: 'Meno',
        accessor: 'name',
      },
      {
        Header: 'Licencia',
        accessor: 'licence',
      }
    ],
    []
  );

  return (
    <>
      <Compoments.Navigation.Bar
        tabs={isMobile}
        footer={<p style={{ color: 'white' }}>Odhlasit sa</p>}
        header={<img src={MainLogo} alt='' />}
        mobileHeader={<><img src={MobileLogo} alt='' /> <p>O</p></>}
      >
        <Compoments.Navigation.Item>
          Uživatelia
        </Compoments.Navigation.Item>

        <Compoments.Navigation.ItemGroup>

          <Compoments.Navigation.Item>
            Vytvoriť Uživatelia
          </Compoments.Navigation.Item>

          <Compoments.Navigation.Item>
            Hromadný Import
          </Compoments.Navigation.Item>

        </Compoments.Navigation.ItemGroup>

        <Compoments.Navigation.Item>
          Licence
        </Compoments.Navigation.Item>

        <Compoments.Navigation.Item>
          Export
        </Compoments.Navigation.Item>

        <Compoments.Navigation.Item>
          Nápoveda
        </Compoments.Navigation.Item>

      </Compoments.Navigation.Bar>
      <Compoments.Content.Container
        header={<p>Zoznam Uzivatelov</p>}
        offSet={!isMobile}
      >
        {
          isMobile ?

            data.map(item =>
              <Compoments.Card
              key={item.id}
                header={<>
                  <div><p className='id-text'><strong>ID </strong>{item.id}</p> <p className='email-text'>{item.email}</p></div>
                  <p><strong>Meno </strong> {item.name}</p>
                </>}
                body={<p><strong>Licencia </strong> {item.licence}</p>}
              />
            )
            :
            <Compoments.Table
              columns={tableColumns}
              data={data}
              header={<p>Vybrany Uživatelia</p>}
            />

        }

      </Compoments.Content.Container>
    </>
  );
}

export default App;
