import React from 'react';
import PropTypes from 'prop-types';
import './card.scss'

const Card = ({ header, body }) => {
    return (
        <div className='card'>
            <div className='card-header'>{header}</div>
            <div className='card-line' />
            <div className='card-body'>{body}</div>
        </div>
    );
}

Card.propTypes = {
    header: PropTypes.element,
    body:  PropTypes.element,
}

export default Card