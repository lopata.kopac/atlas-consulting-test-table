import React, { useContext } from 'react';
import NavigacionContext from './nav-context';
import './item.scss';

const ItemGroup = ({ children }) => {
    const { tabs } = useContext(NavigacionContext);

    return (
        <button className={tabs ? 'navigacion-bar-item-mobile' : 'navigacion-bar-item-destop'}>
            {children}
        </button >
    );
}

export default ItemGroup;