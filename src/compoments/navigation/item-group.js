import React, { useContext } from 'react';
import NavigacionContext from './nav-context';
import './item-group.scss';

const NavigationItemGroup = ({ children }) => {
    const { tabs } = useContext(NavigacionContext);
    if (tabs) return <></>;
    return (
        <div className='navigation-item-group-destop'>
            {children}
        </div>
    );
}

export default NavigationItemGroup;