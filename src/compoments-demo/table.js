import React from 'react';
import Compoments from '../compoments';

import '../app/index.scss';

const TableDemo = () => {
    const columns = React.useMemo(
        () => [
            {
                Header: 'First Name',
                accessor: 'firstName',
            },
            {
                Header: 'Last Name',
                accessor: 'lastName',
            }
        ],
        []
    );

    const data = React.useMemo(() => [
        { firstName: 'fds', lastName: 'fssdfd' },
        { firstName: 'dfdfs', lastName: 'sfd' },
        { firstName: 'dsdfs', lastName: 'sdfsdf' },
        { firstName: 'fds', lastName: 'fssdfd' },
        { firstName: 'dfdfs', lastName: 'sfd' },
        { firstName: 'dsdfs', lastName: 'sdfsdf' },
        { firstName: 'fds', lastName: 'fssdfd' },
        { firstName: 'dfdfs', lastName: 'sfd' },
        { firstName: 'dsdfs', lastName: 'sdfsdf' },
        { firstName: 'fds', lastName: 'fssdfd' },
        { firstName: 'dfdfs', lastName: 'sfd' },
        { firstName: 'dsdfs', lastName: 'sdfsdf' },
    ],
    []);

    return (
        <>
            <Compoments.Table 
            columns={columns} 
            data={data} 
            header={<p>Vybrany Uživatelia</p>}
            />
        </>
    )
}

export default TableDemo;